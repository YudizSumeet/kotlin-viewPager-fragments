package com.viewpagerwithfrag

import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter


class CustomPageAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): android.support.v4.app.Fragment {

        var fragment: android.support.v4.app.Fragment? = null
        when (position) {
            0 -> fragment = Frag1()
            1 -> fragment = Frag2()
        }

        return fragment!!
    }

    override fun getCount(): Int {
        return 2           //no of cases/fragments
    }

}