package com.viewpagerwithfrag

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var customPageAdapter: CustomPageAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
        setAdapter()
    }

    internal fun init() {

        customPageAdapter = CustomPageAdapter(supportFragmentManager)

    }

    internal fun setAdapter() {

        actMain_viewPager!!.adapter = customPageAdapter

    }

}
